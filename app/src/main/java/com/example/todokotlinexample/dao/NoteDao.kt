package com.example.todokotlinexample.dao

import com.example.todokotlinexample.base.entity.NoteEntity
import io.objectbox.android.ObjectBoxLiveData

/**
 * Created by Dhimas Saputra on 22/12/20
 * Jakarta, Indonesia.
 */
interface NoteDao {
    fun save(noteEntity: NoteEntity)
    fun delete(noteEntity: NoteEntity)
    fun getAllNote(): ObjectBoxLiveData<NoteEntity>
    fun getAllNotes(): List<NoteEntity>
    fun deleteAll()
    fun getAllNotes1(limit: Long): List<NoteEntity>?
}