package com.example.todokotlinexample.base

import android.content.Context
import android.util.Log
import com.example.todokotlinexample.base.entity.MyObjectBox
import io.objectbox.BoxStore
import io.objectbox.android.AndroidObjectBrowser

/**
 * Created by Dhimas Saputra on 22/12/20
 * Jakarta, Indonesia.
 */
object ObjectBox {
    lateinit var boxStore: BoxStore

    fun init(context: Context) {
        boxStore = MyObjectBox.builder()
            .androidContext(context.applicationContext)
            .build()



        if (com.example.todokotlinexample.BuildConfig.DEBUG) {

            // Enable Data Browser on debug builds.
            // https://docs.objectbox.io/data-browser
            val started =   AndroidObjectBrowser(boxStore).start(context.applicationContext)
            Log.d("ObjectBoxStarted ",started.toString())

        }
    }

}
