package com.example.todokotlinexample.base.entity

import io.objectbox.annotation.BaseEntity
import io.objectbox.annotation.Id
import java.util.*

/**
 * Created by Dhimas Saputra on 22/12/20
 * Jakarta, Indonesia.
 */

@BaseEntity
abstract class BaseEntity {
    @Id
    var id: Long = 0
    var createdBy: String? = null
    var createdDate: Date? = null
    var updatedBy: String? = null
    var updatedDate: Date? = null


    constructor()
}