package com.example.todokotlinexample.base

import android.app.Application

/**
 * Created by Dhimas Saputra on 22/12/20
 * Jakarta, Indonesia.
 */
class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        ObjectBox.init(this)
    }
}