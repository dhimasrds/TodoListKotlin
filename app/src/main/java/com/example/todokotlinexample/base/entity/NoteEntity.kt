package com.example.todokotlinexample.base.entity

import android.os.Parcelable
import io.objectbox.annotation.Entity
import kotlinx.parcelize.Parcelize

/**
 * Created by Dhimas Saputra on 22/12/20
 * Jakarta, Indonesia.
 */

@Parcelize
@Entity
 data class NoteEntity(
  var title: String?,
  var username: String?,
  var priority: Int,
  var timestamp: Long,
 ) : BaseEntity(), Parcelable