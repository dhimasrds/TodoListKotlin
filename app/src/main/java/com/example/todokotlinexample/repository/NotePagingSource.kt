package com.example.todokotlinexample.repository

import android.util.Log
import androidx.paging.PagingSource
import com.example.todokotlinexample.base.entity.NoteEntity
import com.example.todokotlinexample.dao.NoteDao
import com.example.todokotlinexample.dao.NoteDaoImp
import java.io.IOException
import java.util.*

private const val FIRST_INDEX = 0


class NotePagingSource(
) : PagingSource<Int, NoteEntity>() {
    private val noteDao: NoteDao
    private val repository: NoteRepository

    init {
        noteDao = NoteDaoImp()
        repository = NoteRepository(noteDao)
    }


    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, NoteEntity> {
        val position = params.key ?: FIRST_INDEX
        Log.d("position", position.toString())

        return try {
            val listNote = mutableListOf<NoteEntity>()
            val list = repository.getAllNotes1(position.toLong())
            list?.let { listNote.addAll(it) }
            Log.d("size", listNote.size.toString())

            LoadResult.Page(
                data = listNote,
                prevKey = if (position == FIRST_INDEX) null else position,
                nextKey = if (position > 50) null else position + 3

            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        }
    }

}
