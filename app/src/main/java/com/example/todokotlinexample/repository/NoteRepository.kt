package com.example.todokotlinexample.repository

import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import com.example.todokotlinexample.base.entity.NoteEntity
import com.example.todokotlinexample.dao.NoteDao

/**
 * Created by Dhimas Saputra on 28/12/20
 * Jakarta, Indonesia.
 */
class NoteRepository(val noteDao: NoteDao) {

    suspend fun insert(noteEntity: NoteEntity) = noteDao.save(noteEntity)

    suspend fun delete(noteEntity: NoteEntity)= noteDao.delete(noteEntity)

    suspend fun deleteAll() {
        noteDao.deleteAll()
    }

    fun getAllNote() : LiveData<List<NoteEntity>> = noteDao.getAllNote()
    fun getAllNotes() : List<NoteEntity> = noteDao.getAllNotes()
    fun getAllNotes1(offset : Long)  : List<NoteEntity>? = noteDao.getAllNotes1(offset)

}