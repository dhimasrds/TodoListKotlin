package com.example.todokotlinexample.ui.note


import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment

import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController

import com.example.todokotlinexample.R
import com.example.todokotlinexample.base.entity.NoteEntity
import com.example.todokotlinexample.dao.NoteDao
import com.example.todokotlinexample.dao.NoteDaoImp
import com.example.todokotlinexample.databinding.FragmentNoteBinding
import com.example.todokotlinexample.ui.adapter.NoteLoadStateAdapter

import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class NoteFragment : Fragment() {
    private val viewModel: NoteViewModel by viewModels()
    private lateinit var noteAdapter: NoteAdapter
    private val noteDao : NoteDao

    init {
        noteDao = NoteDaoImp()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View {
        val binding = FragmentNoteBinding.inflate(inflater)

        binding.lifecycleOwner = this
        binding.viewModel = viewModel



        noteAdapter = NoteAdapter(NoteAdapter.TaskClickListener { it ->
            findNavController().navigate(
                NoteFragmentDirections.actionTaskFragmentToUpdateFragment(
                    it
                )
            )
        })
         viewLifecycleOwner.lifecycleScope.launch {
                viewModel.listNote.collectLatest {
                    noteAdapter.submitData(it)
                }
            }


        binding.recyclerView.apply {
            binding.recyclerView.adapter = noteAdapter.withLoadStateFooter(
            footer = NoteLoadStateAdapter{noteAdapter.retry()}
            )


        }
        binding.apply {

            floatingActionButton.setOnClickListener {
                findNavController().navigate(R.id.action_taskFragment_to_addFragment)
            }
        }



        return binding.root

    }


}