package com.example.todokotlinexample.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.todokotlinexample.R
import com.example.todokotlinexample.base.entity.NoteEntity
import com.example.todokotlinexample.dao.NoteDao
import com.example.todokotlinexample.dao.NoteDaoImp

class MainActivity : AppCompatActivity() {
    private val noteDao : NoteDao

    init {
        noteDao = NoteDaoImp()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        for (i in 1..50) {
            val noteEntity = NoteEntity(
                "title $i",
                "desc $i",
                2,
                System.currentTimeMillis()
            )

            noteDao.save(noteEntity)

        }
        setupActionBarWithNavController(findNavController(R.id.nav_host_fragment))
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp() || super.onSupportNavigateUp()
    }
}