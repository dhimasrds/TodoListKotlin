package com.example.todokotlinexample.ui.note

import android.annotation.SuppressLint
import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.paging.PagedListAdapter
import androidx.paging.PagingData
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.todokotlinexample.base.entity.NoteEntity
import com.example.todokotlinexample.databinding.CardviewNoteBinding
import kotlin.coroutines.coroutineContext
import java.util.Collections.list as list

/**
 * Created by Dhimas Saputra on 28/12/20
 * Jakarta, Indonesia.
 */

class NoteAdapter(val clickListener:TaskClickListener): PagingDataAdapter<NoteEntity, NoteAdapter.ViewHolder>(NoteDiffCallBack){


    companion object NoteDiffCallBack : DiffUtil.ItemCallback<NoteEntity>(){
        override fun areItemsTheSame(oldItem: NoteEntity, newItem: NoteEntity): Boolean {
            TODO("Not yet implemented")
        }

        override fun areContentsTheSame(oldItem: NoteEntity, newItem: NoteEntity): Boolean {
            TODO("Not yet implemented")
        }
    }

    class ViewHolder(val binding: CardviewNoteBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(noteEntity: NoteEntity, clickListener: TaskClickListener){
            binding.noteEntity = noteEntity
            binding.clickListener = clickListener
            binding.executePendingBindings()

            binding.taskTimestamp.setOnClickListener {
               val toast =  Toast.makeText(it.context ,"sip",Toast.LENGTH_SHORT)
                toast.show()
//                Log.d("Test Toast",)

            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(CardviewNoteBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(current!!,clickListener)
    }



    class TaskClickListener(val clickListener: (noteEntity: NoteEntity) -> Unit){
        fun onClick(noteEntity: NoteEntity) = clickListener(noteEntity)
    }


}