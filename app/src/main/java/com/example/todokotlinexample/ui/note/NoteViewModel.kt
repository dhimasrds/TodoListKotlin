package com.example.todokotlinexample.ui.note

import android.app.Application
import android.content.Context
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.todokotlinexample.base.entity.NoteEntity
import com.example.todokotlinexample.dao.NoteDao
import com.example.todokotlinexample.dao.NoteDaoImp
import com.example.todokotlinexample.repository.NotePagingSource
import com.example.todokotlinexample.repository.NoteRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import kotlin.coroutines.coroutineContext

class NoteViewModel(application: Application) : AndroidViewModel(application) {

    private val noteDao: NoteDao
    private val noteRepository: NoteRepository
    //    val getAllNotes: Flow<PagingData<NoteEntity>> = getMovieListStream()

    //    val getAllNote : LiveData<List<NoteEntity>>
    val getAllNotes = MutableLiveData<List<NoteEntity>>()


    init {
        noteDao = NoteDaoImp()
        noteRepository = NoteRepository(noteDao)
        getAllNotes.value = noteRepository.getAllNotes()
    }


    fun insert(noteEntity: NoteEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            noteRepository.insert(noteEntity)
        }
    }


    fun delete(noteEntity: NoteEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            noteRepository.delete(noteEntity)
        }
    }

    fun update(noteEntity: NoteEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            noteRepository.insert(noteEntity)
        }
    }

    fun deleteAll() {
        viewModelScope.launch(Dispatchers.IO) {
            noteRepository.deleteAll()
        }
    }

    val listNote = Pager(PagingConfig(pageSize = 5, enablePlaceholders = true,)) {
        NotePagingSource()
    }.flow.cachedIn(viewModelScope)

}
