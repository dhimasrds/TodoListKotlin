package com.example.todokotlinexample.ui.update

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.todokotlinexample.R
import com.example.todokotlinexample.base.entity.NoteEntity
import com.example.todokotlinexample.databinding.FragmentUpdateBinding
import com.example.todokotlinexample.ui.note.NoteViewModel

/**
 * Created by Dhimas Saputra on 28/12/20
 * Jakarta, Indonesia.
 */
class UpdateFragment :Fragment() {
private val viewModel : NoteViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val binding = FragmentUpdateBinding.inflate(inflater)
        val args = UpdateFragmentArgs.fromBundle(requireArguments())

        binding.apply {
            updateEdtTask.setText(args.noteEntity.title)
            updateEdtDesc.setText(args.noteEntity.username)
            updateSpinner.setSelection(args.noteEntity.priority!!)

            btnUpdate.setOnClickListener {
                if(TextUtils.isEmpty(updateEdtTask.text)){
                    Toast.makeText(requireContext(), "It's empty!", Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }

                val task_str = updateEdtTask.text
                val priority = updateSpinner.selectedItemPosition
                val desc = updateEdtDesc.text

                val noteEntity = NoteEntity(
                    task_str.toString(),
                    desc.toString(),
                    priority,
                    args.noteEntity.timestamp
                )
                viewModel.insert(noteEntity)
                Toast.makeText(requireContext(), "Updated!", Toast.LENGTH_SHORT).show()
                findNavController().navigate(R.id.action_updateFragment_to_taskFragment)
            }
        }
        return binding.root
    }
}