package com.example.todokotlinexample.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.todokotlinexample.R
import com.example.todokotlinexample.databinding.LoadStateViewBinding

/**
 * Created by Dhimas Saputra on 05/01/21
 * Jakarta, Indonesia.
 */
class NoteLoadStateViewHolder